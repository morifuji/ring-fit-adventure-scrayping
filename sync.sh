docker build -t ringfit-scrayping ./
set DATETIME (date +%Y%m%d_%H%M%S)
set IMAGE xxxxxxxxxxxx

echo $DATETIME
docker tag ringfit-scrayping:latest $IMAGE:$DATETIME
docker push $IMAGE:$DATETIME
echo $DATETIME

kubectl set image -f ./k8s/cronjob.yml ringfit-scrayping=$IMAGE:$DATETIME --local -o yaml > ./k8s/cronjob_.yml
rm -rf ./k8s/cronjob.yml
mv ./k8s/cronjob_.yml ./k8s/cronjob.yml
kubectl apply -f ./k8s/cronjob.yml